import { loaddirectories, loadPokemonList } from 'services';
import { parse } from 'qs';

export default {

  namespace: 'app',

  state: {
    folders: [1,2,3,4,5,6,7],
    pokemonList: []
  },

  subscriptions: {
    setup({ dispatch, history }) {
    },
  },

  effects: {
    *loaddirectories(action, {call, put}) {
      const data = yield call(loaddirectories);
      console.log(data)
    },
    *loadPokemonList(action, {call, put}) {
      const res = yield call(loadPokemonList);
      yield put({type: 'savePokemon', payload: res.data});
    },
    *loadPokemon(action, {call, put}) {
      console.log(action);
      const res = yield call(loadPokemon)
    }
  },

  reducers: {
    save(state, action) {
      console.log(action);
    },
    fetchFolders(state, action) {
      return state;
    },
    savePokemon(state, {payload: {results}}) {
      console.log(results);
      return {
        ...state,
        pokemonList: results
      };
    }
  },

}
