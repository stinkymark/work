import React from 'react';
import PokemonView from 'components/PokemonView';

export default ({location: {query: url }}) => {
  return (
    <div>
      <h1>one PokemonPage</h1>
      <PokemonView url={url} />
    </div>
  );
}
