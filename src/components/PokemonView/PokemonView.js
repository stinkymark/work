import React from 'react';
import fetch from 'dva/fetch';

class PokemonView extends React.Component {

  render () {
    console.log(this.props);
    return (
      <div>
        <h2>PokemonView</h2>
        {this.props.url.url}
      </div>
    )
  }
}

export default PokemonView;
