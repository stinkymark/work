import React from 'react';
import { connect } from 'dva';

import PokemonView from './PokemonView.js';

const mapStateToProps = (state, ownProps) => {
  return {
    pokemonData: state.app.pokemonList.find(({url}) => url === ownProps.url.url)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadPokemonList: (url) => {
      dispatch({
        type: 'app/loadPokemon',
        meta: {
          url
        }
      });
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PokemonView);
