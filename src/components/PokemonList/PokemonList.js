import React from 'react';
import { Link } from 'react-router'

class PokemonList extends React.Component {
  componentWillMount() {
    this.props.loadPokemonList();
  }

  render() {
    return (
      <div>
        {this.props.pokemonList.map(({name, url}, key) => (
          <div key={`pokemonList-element-${key}`}><Link to={{ pathname: '/pokemon', query: { url: url } }}>{name}</Link></div>
        ))}
      </div>
    )
  }
}

export default PokemonList
