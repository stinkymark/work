import React from 'react';
import { connect } from 'dva';

import PokemonList from './PokemonList.js';

const mapStateToProps = (state) => {
  return {
    pokemonList: state.app.pokemonList
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadPokemonList: () => {
      dispatch({type: 'app/loadPokemonList'});
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PokemonList);
